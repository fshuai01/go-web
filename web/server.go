package web

import (
	"net"
	"net/http"
)

// 确保一定实现了Server接口
var _ Server = &HTTPServer{}

type HandleFunc func(ctx Context)

type Server interface {
	// 组合http包的handler
	http.Handler
	Start(add string) error

	// AddRoute 路由注册功能
	// method是HTTP方法
	// path是路由
	// handleFunc是业务逻辑
	// NOTIC 接口定义可以小而美，实现可以复杂一点
	AddRoute(method string, path string, handleFunc HandleFunc)
}

// 提供https功能
//type HTTPSServer struct {
//	HTTPServer
//}

type HTTPServer struct {
}

func (h *HTTPServer) AddRoute(method string, path string, handleFunc HandleFunc) {
	//TODO implement me
}

func (h *HTTPServer) Get(method string, path string, handleFunc HandleFunc) {
	h.AddRoute(http.MethodGet, path, handleFunc)
}

// ServeHTTP 处理请求的入口
func (h *HTTPServer) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	// 框架代码就在这里
	// Context构建
	// 路由匹配
	// 执行业务逻辑
	ctx := &Context{
		Req:  request,
		Resp: writer,
	}
	h.serve(ctx)
}

func (h *HTTPServer) serve(ctx *Context) {
	// 查找路由，执行命中的业务逻辑
}

func (h *HTTPServer) Start(addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	// 在这里可以让用户注册所谓的after start回调
	// 比如往admin注册一下自己这个实例，类似于微服务中注册自己
	// 或者执行一些业务所需的前置条件
	// 这样实现了将端口监听和服务器启动分离
	return http.Serve(l, h)
}

func (h *HTTPServer) Start1(addr string) error {
	return http.ListenAndServe(addr, h)
}
