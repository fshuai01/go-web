package web

import (
	"fmt"
	"testing"
)

func TestServer(t *testing.T) {
	//var h Server // NewServer之类的方法
	h := &HTTPServer{}
	// 这个handler就是我们跟http包的结合点
	// 第二个参数传一个handler，这个handler就可以自己定义了
	h.AddRoute("GET", "/user", func(ctx Context) {
		fmt.Printf("第一件事")
	})

	// 用法一
	//http.ListenAndServe(":8080", h)

	// 用法二 自己手动管
	h.Start(":8080")
}
